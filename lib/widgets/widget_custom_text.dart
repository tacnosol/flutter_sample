import 'package:flutter/cupertino.dart';

class CustomText extends StatefulWidget {
  final String text;
  final double fontSize;

  CustomText({Key key, this.text, this.fontSize}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomText();
}

class _CustomText extends State<CustomText> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          widget.text,
          style: TextStyle(fontSize: widget.fontSize),
        ));
  }
}
