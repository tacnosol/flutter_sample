import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:random/utils/resource_references.dart';
import 'package:random/widgets/widget_app_bar_buttons.dart';
import 'package:random/widgets/widget_profile_bar.dart';

class CurvedAppBar extends StatefulWidget {
  CurvedAppBar({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CurvedAppBar();
}

class _CurvedAppBar extends State<CurvedAppBar> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 5,
              blurRadius: 5,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
          color: color_app_grey,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(60),
            bottomRight: Radius.circular(60),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(60),
                  bottomRight: Radius.circular(60),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 5.0),
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: color_app_bar_primary,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(60),
                      bottomRight: Radius.circular(60),
                    ),
                  ),
                  child: ProfileBar(),
                ),
              ),
            )),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedAppBarButton(
                    title: "Profile",
                    isActive: true,
                  ),
                  RaisedAppBarButton(
                    title: "Portfolio",
                  ),
                  RaisedAppBarButton(
                    title: "Reviews",
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
