import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HireBanner extends StatefulWidget {
  final String text;

  HireBanner({Key key, this.text}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HireBanner();
}

class _HireBanner extends State<HireBanner> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 60,
        width: 120,
        decoration: BoxDecoration(
            color: Color.fromRGBO(1, 102, 225, 1),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(50))),
        child: Center(
          child: Text(
            widget.text,
            style: TextStyle(color: Colors.white, fontSize: 17),
          ),
        ));
  }
}
