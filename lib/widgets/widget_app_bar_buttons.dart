import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:random/utils/resource_references.dart';

class RaisedAppBarButton extends StatefulWidget {
  final String title;
  final bool isActive;

  RaisedAppBarButton({Key key, this.title, this.isActive = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _RaisedAppBarButton();
}

class _RaisedAppBarButton extends State<RaisedAppBarButton> {
  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: MediaQuery.of(context).size.width / 3.5,
      height: 50.0,
      child: RaisedButton(
        color: widget.isActive ? color_app_bar_primary : Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
        onPressed: () {},
        child: Text(
          widget.title,
          style: TextStyle(
              color: widget.isActive ? Colors.white : Colors.black,
              fontSize: 17),
        ),
      ),
    );
  }
}
