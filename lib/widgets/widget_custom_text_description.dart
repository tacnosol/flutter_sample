import 'package:flutter/cupertino.dart';

class CustomTextDescription extends StatefulWidget {
  final String text;

  CustomTextDescription({Key key, this.text}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomTextDescription();
}

class _CustomTextDescription extends State<CustomTextDescription> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(8.0),
        child: Text(widget.text, style: TextStyle(fontSize: 17)));
  }
}
