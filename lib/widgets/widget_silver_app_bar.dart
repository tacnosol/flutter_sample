import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:random/widgets/widget_curved_bar.dart';

class SilverAppBar extends StatefulWidget {
  SilverAppBar({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SilverAppBar();
}

class _SilverAppBar extends State<SilverAppBar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
        pinned: true,
        expandedHeight: MediaQuery.of(context).size.height / 2,
        leading: Icon(
          Icons.arrow_back_ios,
          size: 30,
        ),
        actions: [
          Icon(
            Icons.more_vert_rounded,
            size: 40,
          )
        ],
        flexibleSpace: FlexibleSpaceBar(
            background: Container(
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 12.0),
                color: Colors.white,
                child: CurvedAppBar())));
  }
}
