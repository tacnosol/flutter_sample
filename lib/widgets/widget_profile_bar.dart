import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileBar extends StatefulWidget {
  final String title;
  final bool isActive;

  ProfileBar({Key key, this.title, this.isActive = false}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfileBar();
}

class _ProfileBar extends State<ProfileBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/profile_pic.png",
            height: MediaQuery.of(context).size.height / 4,
          ),
          Text(
            'Mohammad Abdu',
            style: TextStyle(color: Colors.white, fontSize: 25),
          ),
          SizedBox(
            height: 10,
          ),
          Text('Promising artist and composer with official',
              style: TextStyle(color: Colors.white)),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.star,
                color: Colors.orangeAccent,
                size: 18,
              ),
              Text('(4.9)', style: TextStyle(color: Colors.white))
            ],
          )
        ],
      ),
    );
  }
}
