//App Colors
import 'dart:ui';

const color_app_bar_primary = Color(0xFF2e2f44);
const color_app_bar_secondary = Color(0xFF2196f3);
const color_app_grey = Color(0xFFb8b8b8);