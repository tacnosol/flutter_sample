import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:random/widgets/widget_custom_text.dart';
import 'package:random/widgets/widget_custom_text_description.dart';
import 'package:random/widgets/widget_hire_banner.dart';
import 'package:random/widgets/widget_silver_app_bar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SilverAppBar(),
          SliverToBoxAdapter(
            child: Container(
              height: MediaQuery.of(context).size.height / 2.2,
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    text: 'Categories',
                    fontSize: 20,
                  ),
                  CustomText(
                    text: 'Singer, Private concerts, Event converts',
                    fontSize: 18,
                  ),
                  CustomText(
                    text: "About me",
                    fontSize: 20,
                  ),
                  CustomTextDescription(
                    text:
                        'Promising artist and composer with official Promising artist and composer with official Promisingartist and composer with official Promising artistand composer with official Promising artist andcomposer with official',
                  ),
                  Expanded(
                      child: Align(
                          alignment: Alignment.bottomRight,
                          child: HireBanner(
                            text: 'Hire Me',
                          )))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
